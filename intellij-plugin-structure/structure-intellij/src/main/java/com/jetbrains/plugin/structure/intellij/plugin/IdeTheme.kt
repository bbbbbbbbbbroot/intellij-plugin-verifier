package com.jetbrains.plugin.structure.intellij.plugin

data class IdeTheme(val name: String, val dark: Boolean)